<?php
class MovimentacaoSerializer {

	public function serializeList($movimentacoes) {
		$movimentacoes_array = array();
		foreach ($movimentacoes as $movimentacao) {
			$movimentacoes_array[] = $this->serialize($movimentacao);
		}
		return $movimentacoes_array;
	}

	public function serialize($movimentacaoModel) {
		return array(
			"id" => $movimentacaoModel->getId(),
			"tipo" => $movimentacaoModel->getTipo(),
			"categoria" => $movimentacaoModel->getCategoria(),
			"data" => $movimentacaoModel->getData(),
			"valor" => $movimentacaoModel->getValor(),
			"descricao" => $movimentacaoModel->getDescricao()
		);
	}

}
