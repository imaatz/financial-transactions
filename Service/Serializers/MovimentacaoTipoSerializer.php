<?php
class MovimentacaoTipoSerializer {

	public function serializeList($movimentacaoTipos) {
		$movimentacaoTipos_array = array();
		foreach ($movimentacaoTipos as $movimentacaoTipo) {
			$movimentacaoTipos_array[] = $this->serialize($movimentacaoTipo);
		}
		return $movimentacaoTipos_array;
	}

	public function serialize($movimentacaoTipoModel) {
		return array(
			"id" => $movimentacaoTipoModel->getId(),
			"descricao" => $movimentacaoTipoModel->getDescricao(),
		);
	}

}
