<?php
require "Model.php";

class MovimentacaoTipoModel extends Model {

    private $id;
    private $descricao;

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

    public function getDescricao() {
        return $this->descricao;
    }
	  public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }


    public function valida() {
        $error = true;

        if ($this->getDescricao() == "" || strlen($this->getDescricao())>50) {
            Message::setMessage('descricao', 'Descrição inválida', 'error');
            $error = false;
        }

        return $error;
    }

}
