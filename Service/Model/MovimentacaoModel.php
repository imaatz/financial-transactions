<?php
require_once "Model.php";

class MovimentacaoModel extends Model {

		private $id;
    private $tipo;
    private $categoria;
    private $data;
    private $valor;
    private $descricao;

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

    public function getTipo() {
        return $this->tipo;
    }
    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    public function getCategoria() {
        return $this->categoria;
    }
    public function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    public function getData() {
        return $this->data;
    }
    public function setData($data) {
        $this->data = $data;
    }

    public function getValor() {
        return $this->valor;
    }
    public function setValor($valor) {
        $this->valor = $valor;
    }

    public function getDescricao() {
        return $this->descricao;
    }
    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function valida() {
        $error = false;

        if ($this->getTipo() == "") {
            // Message::setMessage('tipo', 'Tipo inválido', 'error');
            $error = true;
        }

        if ($this->getCategoria() == "") {
            // Message::setMessage('categoria', 'Categoria inválida', 'error');
            $error = true;
        }

        if ($this->getData() == "") {
            // Message::setMessage('data', 'Data inválida', 'error');
            $error = true;
        }

        if ($this->getValor() < 0 || $this->getValor() > 9999999999.99) {
            // Message::setMessage('valor', 'Valor inválido', 'error');
            $error = true;
        }

        if ($this->getDescricao() == "" || strlen($this->getDescricao())>50) {
            // Message::setMessage('descricao', 'Descrição inválida', 'error');
            $error = true;
        }

        return $error;
    }

}
