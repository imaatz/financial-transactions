<?php
require_once "DAO.php";

class MovimentacaoDAO extends DAO {

    public function getById($id) {
        $stmt = $this->conn->prepare("
          SELECT *
            FROM movimentacao
            WHERE id = :id
            ORDER BY data DESC
        ");
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        if ($stmt) {
            $row = $stmt->fetchObject();
            $movimentacaoModel = new MovimentacaoModel;
            $movimentacaoModel->setId($row->ID);
            $movimentacaoModel->setTipo($row->TIPO);
            $movimentacaoModel->setCategoria($row->CATEGORIA);
            $movimentacaoModel->setData($row->DATA);
            $movimentacaoModel->setValor($row->VALOR);
            $movimentacaoModel->setDescricao($row->DESCRICAO);
        }
        return $movimentacaoModel;
    }

    public function getByCategoria($categoria) {
        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM movimentacao
            WHERE categoria = :categoria
            ORDER BY data DESC
        ");
        $stmt->bindValue(':categoria', $categoria);
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $movimentacaoModel = new MovimentacaoModel;
            $movimentacaoModel->setId($row->ID);
            $movimentacaoModel->setTipo($row->TIPO);
            $movimentacaoModel->setCategoria($row->CATEGORIA);
            $movimentacaoModel->setData($row->DATA);
            $movimentacaoModel->setValor($row->VALOR);
            $movimentacaoModel->setDescricao($row->DESCRICAO);
            $results[] = $movimentacaoModel;
          }
        }
        return $results;
    }

    public function getByCategoriaMes($categoria, $mes, $ano) {
        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM movimentacao
            WHERE categoria = :categoria
              AND data like :mesAno
            ORDER BY data DESC
        ");
        $stmt->bindValue(':categoria', $categoria);
        $stmt->bindValue(':mesAno', $ano . '-' . $mes . '-__ __:__:__');
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $movimentacaoModel = new MovimentacaoModel;
            $movimentacaoModel->setId($row->ID);
            $movimentacaoModel->setTipo($row->TIPO);
            $movimentacaoModel->setCategoria($row->CATEGORIA);
            $movimentacaoModel->setData($row->DATA);
            $movimentacaoModel->setValor($row->VALOR);
            $movimentacaoModel->setDescricao($row->DESCRICAO);
            $results[] = $movimentacaoModel;
          }
        }
        return $results;
    }

    public function getByMes($mes, $ano) {
        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM movimentacao
            WHERE data like :mesAno
            ORDER BY data DESC
        ");
        $stmt->bindValue(':mesAno', $ano . '-' . $mes . '-__ __:__:__');
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $movimentacaoModel = new MovimentacaoModel;
            $movimentacaoModel->setId($row->ID);
            $movimentacaoModel->setTipo($row->TIPO);
            $movimentacaoModel->setCategoria($row->CATEGORIA);
            $movimentacaoModel->setData($row->DATA);
            $movimentacaoModel->setValor($row->VALOR);
            $movimentacaoModel->setDescricao($row->DESCRICAO);
            $results[] = $movimentacaoModel;
          }
        }
        return $results;
    }

    public function insert(MovimentacaoModel $movimentacaoModel) {
        try {
            $stmt = $this->conn->prepare("
              INSERT INTO movimentacao (tipo, categoria, data, valor, descricao)
                VALUES (:tipo, :categoria, :data, :valor, :descricao)
            ");
            $stmt->bindValue(':tipo', $movimentacaoModel->getTipo());
            $stmt->bindValue(':categoria', $movimentacaoModel->getCategoria());
            $stmt->bindValue(':data', $movimentacaoModel->getData());
            $stmt->bindValue(':valor', $movimentacaoModel->getValor());
            $stmt->bindValue(':descricao', $movimentacaoModel->getDescricao());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function update(MovimentacaoModel $movimentacaoModel) {
        try {
            $stmt = $this->conn->prepare("
              UPDATE movimentacao
                SET tipo = :tipo,
                    categoria = :categoria,
                    data = :data,
                    valor = :valor,
                    descricao = :descricao
                WHERE id = :id
            ");
            $stmt->bindValue(':id', $movimentacaoModel->getId());
            $stmt->bindValue(':tipo', $movimentacaoModel->getTipo());
            $stmt->bindValue(':categoria', $movimentacaoModel->getCategoria());
            $stmt->bindValue(':data', $movimentacaoModel->getData());
            $stmt->bindValue(':valor', $movimentacaoModel->getValor());
            $stmt->bindValue(':descricao', $movimentacaoModel->getDescricao());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function delete($id) {
        $this->conn->beginTransaction();
        try {
            $stmt = $this->conn->prepare('
                DELETE FROM movimentacao
                WHERE id = :id
            ');
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $this->conn->commit();
        } catch (Exception $e) {
            $this->conn->rollback();
        }

        return true;
    }

}
