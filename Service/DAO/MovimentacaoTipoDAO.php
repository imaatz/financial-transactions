<?php
require "DAO.php";

class MovimentacaoTipoDAO extends DAO {

    public function getAll() {
        $results = array();
        $stmt = $this->conn->query("
          SELECT *
            FROM tipos_movimentacao
        ");
        if ($stmt) {
            while ($row = $stmt->fetchObject()) {
                $tipos_movimentacaoModel = new MovimentacaoTipoModel;
                $tipos_movimentacaoModel->setId($row->ID);
                $tipos_movimentacaoModel->setDescricao($row->DESCRICAO);
                $results[] = $tipos_movimentacaoModel;
            }
        }
        return $results;
    }

    public function getById($id) {
        $stmt = $this->conn->prepare("
          SELECT *
            FROM tipos_movimentacao
            WHERE id = :id
        ");
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        if ($stmt) {
            $row = $stmt->fetchObject();
            $tipos_movimentacaoModel = new MovimentacaoTipoModel;
            $tipos_movimentacaoModel->setId($row->ID);
            $tipos_movimentacaoModel->setDescricao($row->DESCRICAO);
        }
        return $tipos_movimentacaoModel;
    }

    public function insert(MovimentacaoTipoModel $tipos_movimentacaoModel) {
        try {
            $stmt = $this->conn->prepare("
              INSERT INTO tipos_movimentacao (descricao)
                VALUES (:descricao)
            ");
            $stmt->bindValue(':descricao', $tipos_movimentacaoModel->getDescricao());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function update(MovimentacaoTipoModel $tipos_movimentacaoModel) {
        try {
            $stmt = $this->conn->prepare("
              UPDATE tipos_movimentacao
                SET descricao = :descricao
                WHERE id = :id
            ");
            $stmt->bindValue(':id', $tipos_movimentacaoModel->getId());
            $stmt->bindValue(':descricao', $tipos_movimentacaoModel->getDescricao());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function delete($id) {
        $this->conn->beginTransaction();
        try {
            $stmt = $this->conn->prepare('
                DELETE FROM tipos_movimentacao
                WHERE id = :id
            ');
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $this->conn->commit();
        } catch (Exception $e) {
            $this->conn->rollback();
        }
        return true;
    }

}
