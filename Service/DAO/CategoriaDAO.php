<?php
require_once "DAO.php";

class CategoriaDAO extends DAO {

    public function getAll() {
        $results = array();
        $stmt = $this->conn->query("
          SELECT *
            FROM categoria
        ");
        if ($stmt) {
            while ($row = $stmt->fetchObject()) {
                $categoriaModel = new CategoriaModel;
                $categoriaModel->setId($row->ID);
                $categoriaModel->setDescricao($row->DESCRICAO);
                $results[] = $categoriaModel;
            }
        }
        return $results;
    }

    public function getById($id) {
        $stmt = $this->conn->prepare("
          SELECT *
            FROM categoria
            WHERE id = :id
        ");
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        if ($stmt) {
            $row = $stmt->fetchObject();
            $categoriaModel = new CategoriaModel;
            $categoriaModel->setId($row->ID);
            $categoriaModel->setDescricao($row->DESCRICAO);
        }
        return $results;
    }

    public function insert(CategoriaModel $categoriaModel) {
        try {
            $stmt = $this->conn->prepare("
              INSERT INTO categoria (descricao)
                VALUES (:descricao)
            ");
            $stmt->bindValue(':descricao', $categoriaModel->getDescricao());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function update(CategoriaModel $categoriaModel) {
        try {
            $stmt = $this->conn->prepare("
              UPDATE categoria
                SET descricao = :descricao
                WHERE id = :id
            ");
            $stmt->bindValue(':id', $categoriaModel->getId());
            $stmt->bindValue(':descricao', $categoriaModel->getDescricao());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function delete($id) {
        $this->conn->beginTransaction();
        try {
            $stmt = $this->conn->prepare('
                DELETE FROM categoria
                WHERE id = :id
            ');
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $this->conn->commit();
        } catch (Exception $e) {
            $this->conn->rollback();
        }
        return true;
    }

}
