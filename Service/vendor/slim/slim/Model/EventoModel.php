﻿<?php

require_once "Model.php";

class EventoModel extends Model {

    private $id;
    private $descricao;
    private $titulo;
    private $dataHora;

    public function getId() {
        return $this->id;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getDataHora() {
        return $this->dataHora;
    }
    
    public function getData() {
        $data = explode(" ", $this->dataHora);
        return $data[0];
    }
    
    public function getHora() {
        $hora = explode(" ", $this->dataHora);
        $hora = explode(":", $hora[1]);
        return $hora[0] . ":" . $hora[1];
    }
	
	
	
    public function setId($id) {
        $this->id = $id;
    }
	
    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setDataHora($dataHora) {
        $this->dataHora = $dataHora;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }	
	
	
	
    public function valida() {
        $error = true;

        if ($this->getTitulo() == "") {
            Message::setMessage('titulo', 'Titulo inválido', 'error');
            $error = false;
        }

        if ($this->getDescricao() == "") {
            Message::setMessage('descricao', 'Descricao inválido', 'error');
            $error = false;
        }

        if ($this->getDataHora() == "") {
            Message::setMessage('tipo', 'Nome de usuário inválido', 'error');
            $error = false;
        }

        return $error;
    }

}