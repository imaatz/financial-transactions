﻿<?php

class UsuarioModel extends Model {

    private $id;
    private $nome;
    private $email;
    private $telefone;
    private $tipo;
    private $username;
    private $senha;
    private $perfil;

    public function getId() {
        return $this->id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getTelefone() {
        return $this->telefone;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getSenha() {
        return $this->senha;
    }

    public function getPerfil() {
        return $this->perfil;
    }
    
    
    
    public function setId($id) {
        $this->id = $id;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }
    
    public function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setSenha($senha) {
        $this->senha = $senha;
    }

    public function setEmail($email) {
        $this->email = $email;
    }   

    public function setPerfil($perfil) {
        $this->perfil = $perfil;
    }   
    
    
    
    public function valida() {
        $error = true;

        if ($this->getNome() == "" || substr_count($this->getNome(), ' ', 0) > 80) {
            Message::setMessage('nome', 'Nome inválido', 'error');
            $error = false;
        }

        if ($this->getTelefone() == "") {
            Message::setMessage('telefone', 'Telefone inválido', 'error');
            $error = false;
        }

        if ($this->getEmail() == "") {
            Message::setMessage('email', 'Email inválido', 'error');
            $error = false;
        }

        if ($this->getUsername() == "") {
            Message::setMessage('tipo', 'Nome de usuário inválido', 'error');
            $error = false;
        }

        if ($this->getSenha() == "") {
            Message::setMessage('senha', 'Senha inválida', 'error');
            $error = false;
        }

        return $error;
    }

}