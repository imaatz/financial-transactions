﻿<?php

class ObrigacaoModel extends Model {

    private $id;
    private $descricao;
    private $titulo;

    public function getId() {
        return $this->id;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getDescricao() {
        return $this->descricao;
    }
	
	
    public function setId($id) {
        $this->id = $id;
    }
	
    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }	
	
	
    public function valida() {
        $error = true;

        if ($this->getTitulo() == "") {
            Message::setMessage('titulo', 'Titulo inválido', 'error');
            $error = false;
        }

        if ($this->getDescricao() == "") {
            Message::setMessage('descricao', 'Descricao inválido', 'error');
            $error = false;
        }

        return $error;
    }

}