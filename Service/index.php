<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

$app = new \Slim\App;

//GET
$app->get('/form', function (Request $request, Response $response) {
	require 'DAO/MovimentacaoTipoDAO.php';
	require 'Serializers/MovimentacaoTipoSerializer.php';
	require 'Model/MovimentacaoTipoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();
	$movimentacaoTipoDAO = new MovimentacaoTipoDAO();
	$movimentacaoTipoSerializer = new MovimentacaoTipoSerializer();
	$movimentacaoTipoModel = new MovimentacaoTipoModel();

	$tipos = $movimentacaoTipoSerializer->serializeList($movimentacaoTipoDAO->getAll());
	$categorias = $categoriaSerializer->serializeList($categoriaDAO->getAll());

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('tipos' => $tipos, 'categorias' => $categorias, 'movimentacao' => 0));
});

$app->get('/form/{id}', function (Request $request, Response $response) {
	$id = $request->getAttribute('id');
	require 'DAO/MovimentacaoTipoDAO.php';
	require 'Serializers/MovimentacaoTipoSerializer.php';
	require 'Model/MovimentacaoTipoModel.php';
	require 'DAO/MovimentacaoDAO.php';
	require 'Serializers/MovimentacaoSerializer.php';
	require 'Model/MovimentacaoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';
	$movimentacaoModel = new MovimentacaoModel();
	$movimentacaoSerializer = new MovimentacaoSerializer();
	$movimentacaoDAO = new MovimentacaoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();
	$movimentacaoTipoDAO = new MovimentacaoTipoDAO();
	$movimentacaoTipoSerializer = new MovimentacaoTipoSerializer();
	$movimentacaoTipoModel = new MovimentacaoTipoModel();

	$tipos = $movimentacaoTipoSerializer->serializeList($movimentacaoTipoDAO->getAll());
	$categorias = $categoriaSerializer->serializeList($categoriaDAO->getAll());
	$movimentacao = $movimentacaoSerializer->serialize($movimentacaoDAO->getById($id));

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('tipos' => $tipos, 'categorias' => $categorias, 'movimentacao' => $movimentacao));
});

$app->get('/movimentacoes', function (Request $request, Response $response) {
	require 'DAO/MovimentacaoDAO.php';
	require 'Serializers/MovimentacaoSerializer.php';
	require 'Model/MovimentacaoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';
	$movimentacaoModel = new MovimentacaoModel();
	$movimentacaoSerializer = new MovimentacaoSerializer();
	$movimentacaoDAO = new MovimentacaoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();

	$mes = date('m');
	$ano = date('Y');
	$movimentacoes = $movimentacaoSerializer->serializeList($movimentacaoDAO->getByMes($mes, $ano));
	$categorias = $categoriaSerializer->serializeList($categoriaDAO->getAll());

	$meses = array();
	for($i=0; $i>=-10; $i--) {
		$meses[] = date('m/Y', strtotime($i . ' months', strtotime(date('Y-m-d'))));
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('movimentacoes' => $movimentacoes, 'categorias' => $categorias, 'meses' => $meses));
});

$app->get('/movimentacao/{id}', function (Request $request, Response $response) {
	$id = $request->getAttribute('id');
	require 'DAO/MovimentacaoDAO.php';
	require 'Serializers/MovimentacaoSerializer.php';
	require 'Model/MovimentacaoModel.php';
	$movimentacaoModel = new MovimentacaoModel();
	$movimentacaoSerializer = new MovimentacaoSerializer();
	$movimentacaoDAO = new MovimentacaoDAO();

	$movimentacao = $movimentacaoSerializer->serialize($movimentacaoDAO->getById($id));

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('movimentacao' => $movimentacao));
});

$app->get('/movimentacoes/categoria/{categoria}', function (Request $request, Response $response) {
	$categoria = $request->getAttribute('categoria');
	if($categoria > 12) {
		$categoria = 12;
	} else if($categoria < 1) {
		$categoria = 1;
	}
	require 'DAO/MovimentacaoDAO.php';
	require 'Serializers/MovimentacaoSerializer.php';
	require 'Model/MovimentacaoModel.php';
	$movimentacaoModel = new MovimentacaoModel();
	$movimentacaoSerializer = new MovimentacaoSerializer();
	$movimentacaoDAO = new MovimentacaoDAO();

	$movimentacoes = $movimentacaoSerializer->serializeList($movimentacaoDAO->getByCategoria($categoria));

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('movimentacoes' => $movimentacoes));
});

$app->get('/movimentacoes/categoria/{categoria}/{ano}/{mes}', function (Request $request, Response $response) {
	$categoria = $request->getAttribute('categoria');
	$mes = $request->getAttribute('mes');
	$ano = $request->getAttribute('ano');
	if($categoria > 12) {
		$categoria = 12;
	} else if($categoria < 1) {
		$categoria = 1;
	}
	if(!in_array($mes, ['01','02','03','04','05','06','07','08','09','10','11','12'])) {
		$mes = '12';
	}
	require 'DAO/MovimentacaoDAO.php';
	require 'Serializers/MovimentacaoSerializer.php';
	require 'Model/MovimentacaoModel.php';
	$movimentacaoModel = new MovimentacaoModel();
	$movimentacaoSerializer = new MovimentacaoSerializer();
	$movimentacaoDAO = new MovimentacaoDAO();

	$movimentacoes = $movimentacaoSerializer->serializeList($movimentacaoDAO->getByCategoriaMes($categoria, $mes, $ano));

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('movimentacoes' => $movimentacoes));
});

$app->get('/movimentacoes/mes/{ano}/{mes}', function (Request $request, Response $response) {
	$mes = $request->getAttribute('mes');
	$ano = $request->getAttribute('ano');
	if($mes > 12) {
		$mes = 12;
	} else if($mes < 1) {
		$mes = 1;
	}
	if($ano > (int) date('Y')) {
		$ano = (int) date('Y');
	}
	require 'DAO/MovimentacaoDAO.php';
	require 'Serializers/MovimentacaoSerializer.php';
	require 'Model/MovimentacaoModel.php';
	$movimentacaoModel = new MovimentacaoModel();
	$movimentacaoSerializer = new MovimentacaoSerializer();
	$movimentacaoDAO = new MovimentacaoDAO();

	$movimentacoes = $movimentacaoSerializer->serializeList($movimentacaoDAO->getByMes($mes, $ano));

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('movimentacoes' => $movimentacoes));
});


//POST
$app->post('/movimentacoes/insert', function (Request $request, Response $response) {
	$json = $request->getBody();
	$data = json_decode($json, true);

	require 'DAO/MovimentacaoDAO.php';
	require 'Serializers/MovimentacaoSerializer.php';
	require 'Model/MovimentacaoModel.php';
	$movimentacaoDAO = new MovimentacaoDAO();

	$movimentacaoModel = new MovimentacaoModel();
	$movimentacaoModel->setTipo($data["tipo"]);
	$movimentacaoModel->setCategoria($data["categoria"]);
	$movimentacaoModel->setData($data["data"]);
	$movimentacaoModel->setValor($data["valor"]);
	$movimentacaoModel->setDescricao($data["descricao"]);

	$erro = array();
	$valida = $movimentacaoModel->valida();
	if($valida==1){
		$erro[] = "Erro ao cadastrar movimentação";
	} else {
		$movimentacaoDAO->insert($movimentacaoModel);
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson($erro);
});

$app->post('/movimentacoes/update/{id}', function (Request $request, Response $response) {
	$json = $request->getBody();
	$data = json_decode($json, true);
	$id = $request->getAttribute('id');

	require 'DAO/MovimentacaoDAO.php';
	require 'Serializers/MovimentacaoSerializer.php';
	require 'Model/MovimentacaoModel.php';
	$movimentacaoDAO = new MovimentacaoDAO();

	$movimentacaoModel = new MovimentacaoModel();
	$movimentacaoModel->setId($id);
	$movimentacaoModel->setTipo($data["tipo"]);
	$movimentacaoModel->setCategoria($data["categoria"]);
	$movimentacaoModel->setData($data["data"]);
	$movimentacaoModel->setValor($data["valor"]);
	$movimentacaoModel->setDescricao($data["descricao"]);

	$erro = array();
	$valida = $movimentacaoModel->valida();
	if($valida==1){
		$erro[] = "Erro ao cadastrar movimentação";
	} else {
		$movimentacaoDAO->update($movimentacaoModel);
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson($erro);
});

$app->post('/movimentacoes/delete/{id}', function (Request $request, Response $response) {
	$id = $request->getAttribute('id');

	require 'DAO/MovimentacaoDAO.php';
	require 'Serializers/MovimentacaoSerializer.php';
	require 'Model/MovimentacaoModel.php';
	$movimentacaoModel = new MovimentacaoModel();
	$movimentacaoSerializer = new MovimentacaoSerializer();
	$movimentacaoDAO = new MovimentacaoDAO();

	if($id==null){
		$erro[] = "Erro ao excluir movimentação";
	} else {
		$movimentacaoDAO->delete($id);;
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson($erro);
});

$app->run();
